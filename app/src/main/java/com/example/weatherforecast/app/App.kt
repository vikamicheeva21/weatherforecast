package com.example.weatherforecast.app

import android.app.Application
import com.example.weatherforecast.di.components.DaggerAppComponent
import com.example.weatherforecast.di.components.DaggerCoreComponent

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        val appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        AppComponentsHolder.coreComponent = DaggerCoreComponent.builder()
            .appComponent(appComponent)
            .build()
    }
}