package com.example.weatherforecast.app

import com.example.weatherforecast.di.components.CoreComponent
import com.example.weatherforecast.di.components.DaggerMainComponent
import com.example.weatherforecast.di.components.MainComponent

object AppComponentsHolder {
    internal var coreComponent: CoreComponent? = null
    private var mainComponent: MainComponent? = null

    fun getMainComponent(): MainComponent {
        if (mainComponent == null) {
            initMainComponent()
        }
        return mainComponent!!
    }

    fun initMainComponent() {
        mainComponent = null
        mainComponent = DaggerMainComponent.builder()
            .coreComponent(coreComponent)
            .build()
    }

    fun releaseMainComponent() {
        mainComponent = null
    }
}