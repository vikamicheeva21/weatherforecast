package com.example.weatherforecast.data.api

import com.example.weatherforecast.data.models.Weather
import io.reactivex.Single
import retrofit2.http.GET

interface WeatherApiService {
    //TODO переделать по-нормальному:
    //- appid зашить в buildFlavor
    //- lat и lon передавать через аргументы метода. Для сэмпла можно тоже зашить, например, в константы
    @GET("onecall?lat=59.93&lon=30.31&exclude=minutely,alerts&appid=1d9a6c95e3f320118937354389086061")
    fun getWeather(): Single<Weather>
}