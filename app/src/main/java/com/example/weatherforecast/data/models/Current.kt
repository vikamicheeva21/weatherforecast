package com.example.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

data class Current(
    var dt: Long? = null,
    var sunrise: Long? = null,
    var sunset: Long? = null,
    var temp: Double? = null,
    @SerializedName("feels_like")
    var feelsLike: Double? = null,
    var pressure: Long? = null,
    var humidity: Long? = null,
    @SerializedName("dew_point")
    var dewPoint: Double? = null,
    var uvi: Double? = null,
    var clouds: Long? = null,
    var visibility: Long? = null,
    @SerializedName("wind_speed")
    var windSpeed: Double? = null,
    @SerializedName("wind_deg")
    var windDeg: Long? = null,
    var weather: List<WeatherItem>? = null
)