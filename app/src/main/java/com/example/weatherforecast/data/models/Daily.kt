package com.example.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

data class Daily(
    var dt: Long? = null,
    var sunrise: Long? = null,
    var sunset: Long? = null,
    var moonrise: Long? = null,
    var moonset: Long? = null,
    @SerializedName("moon_phase")
    var moonPhase: Double? = null,
    var temp: Temp? = null,
    @SerializedName("feels_like")
    var feelsLike: FeelsLike? = null,
    var pressure: Long? = null,
    var humidity: Long? = null,
    @SerializedName("dew_point")
    var dewPoint: Double? = null,
    @SerializedName("wind_speed")
    var windSpeed: Double? = null,
    @SerializedName("wind_deg")
    var windDeg: Long? = null,
    @SerializedName("wind_gust")
    var windGust: Double? = null,
    var weather: List<WeatherItem>? = null,
    var clouds: Long? = null,
    var pop: Double? = null,
    var rain: Double? = null,
    var uvi:  Double? = null
)