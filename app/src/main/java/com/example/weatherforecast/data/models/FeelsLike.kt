package com.example.weatherforecast.data.models

data class FeelsLike(
    var day: Double? = null,
    var night: Double? = null,
    var eve: Double? = null,
    var morn: Double? = null
)