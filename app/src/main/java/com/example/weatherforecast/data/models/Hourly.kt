package com.example.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

data class Hourly(
    var dt: Long? = null,
    var temp: Double? = null,
    @SerializedName("feels_like")
    var feelsLike: Double? = null,
    var pressure: Long? = null,
    var humidity: Long? = null,
    @SerializedName("dew_point")
    var dewPoint: Double? = null,
    var uvi: Double? = null,
    var clouds: Long? = null,
    var visibility: Long? = null,
    @SerializedName("wind_speed")
    var windSpeed: Double? = null,
    @SerializedName("wind_deg")
    var windDeg: Long? = null,
    @SerializedName("wind_gust")
    var windGust: Double? = null,
    var weather: List<WeatherItem>? = null,
    var pop: Double? = null,
    var rain: Rain? = null
)