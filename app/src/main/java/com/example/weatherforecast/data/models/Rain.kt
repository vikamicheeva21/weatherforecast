package com.example.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

data class Rain(
    @SerializedName("1h")
    val _1h: Double? = null
)