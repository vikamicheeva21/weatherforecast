package com.example.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

data class Weather(
    var lat: Double? = null,
    var lon: Double? = null,
    var timezone: String? = null,
    @SerializedName("timezone_offset")
    var timezoneOffset: Long? = null,
    var current: Current? = null,
    var hourly: List<Hourly>? = null,
    var daily: List<Daily>? = null
)