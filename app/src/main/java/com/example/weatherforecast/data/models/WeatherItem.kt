package com.example.weatherforecast.data.models

data class WeatherItem (
    var id: Long? = null,
    var main: String? = null,
    var description: String? = null,
    var icon: String? = null
)