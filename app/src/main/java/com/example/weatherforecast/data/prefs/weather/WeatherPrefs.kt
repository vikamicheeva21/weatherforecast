package com.example.weatherforecast.data.prefs.weather

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.weatherforecast.data.models.Weather
import com.google.gson.Gson

interface WeatherPrefs {

    var lastUpdate: Long
    var lastWeather: Weather?

}

private const val PREFS_NAME = "weather_prefs"
private const val PREFS_LAST_UPDATE_KEY = "weather_last_update"
private const val PREFS_LAST_VALUE_KEY = "weather_last"

class WeatherPrefsImpl(private val context: Context, private val gson: Gson) : WeatherPrefs {

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
    }
    override var lastUpdate: Long
        get() = prefs.getLong(PREFS_LAST_UPDATE_KEY, 0L)
        set(value) {
            prefs.edit { putLong(PREFS_LAST_UPDATE_KEY, value) }
        }
    override var lastWeather: Weather?
        get() {
            val value = prefs.getString(PREFS_LAST_VALUE_KEY, "")
            return try {
                gson.fromJson(value, Weather::class.java)
            } catch (e: Exception) {
                null
            }
        }
        set(value) {
            prefs.edit {
                putString(PREFS_LAST_VALUE_KEY, value?.let { gson.toJson(value) })
            }
        }
}