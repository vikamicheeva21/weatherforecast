package com.example.weatherforecast.di.components

import com.example.weatherforecast.di.modules.AppModule
import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/// этот компонент провайдит приложение, в основном это Context
@Component(
    modules = [AppModule::class]
)
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun provideContext(): Context
}