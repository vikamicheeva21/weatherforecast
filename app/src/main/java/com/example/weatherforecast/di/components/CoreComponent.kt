package com.example.weatherforecast.di.components

import android.content.Context
import com.example.weatherforecast.di.modules.NetworkModule
import com.example.weatherforecast.di.scopes.CoreScope
import com.google.gson.Gson
import dagger.Component
import retrofit2.Retrofit

// этот компонент провайдит core-зависимости (по идее, лучше выделить в отдельный gradle-модуль),
// например, retrofit, okhttpClient, db  и тд
@Component(
    dependencies = [AppComponent::class],
    modules = [
        NetworkModule::class
    ]
)
@CoreScope
interface CoreComponent {
    fun provideContext(): Context
    fun provideGson(): Gson
    fun provideRetrofit(): Retrofit
}