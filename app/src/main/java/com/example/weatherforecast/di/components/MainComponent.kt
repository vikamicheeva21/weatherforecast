package com.example.weatherforecast.di.components

import com.example.weatherforecast.MainActivity
import com.example.weatherforecast.di.modules.MainModule
import com.example.weatherforecast.di.scopes.MainScope
import com.example.weatherforecast.di.vmBase.VmFactoryWrapper
import dagger.Component

// компонент main-фичи, здесь провайдятся ее viewModels, репозиторий, интерактор
@Component(
    dependencies = [CoreComponent::class],
    modules = [MainModule::class]
)
@MainScope
interface MainComponent {
    fun inject(activity: MainActivity)
    fun inject(vmFactoryWrapper: VmFactoryWrapper)
}