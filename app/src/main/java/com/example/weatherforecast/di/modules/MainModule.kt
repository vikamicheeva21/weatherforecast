package com.example.weatherforecast.di.modules

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.weatherforecast.data.api.WeatherApiService
import com.example.weatherforecast.data.prefs.weather.WeatherPrefs
import com.example.weatherforecast.data.prefs.weather.WeatherPrefsImpl
import com.example.weatherforecast.di.scopes.MainScope
import com.example.weatherforecast.di.vmBase.VmFactory
import com.example.weatherforecast.di.vmBase.VmKeyName
import com.example.weatherforecast.domain.interactor.WeatherInteractor
import com.example.weatherforecast.domain.interactor.WeatherInteractorImpl
import com.example.weatherforecast.domain.repository.WeatherRepository
import com.example.weatherforecast.domain.repository.WeatherRepositoryImpl
import com.example.weatherforecast.presentation.main.MainFragmentVm
import com.google.gson.Gson
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module(includes = [MainModule.BindsModule::class])
class MainModule {

    @MainScope
    @Provides
    fun provideApiService(
        retrofit: Retrofit
    ): WeatherApiService = retrofit.create(WeatherApiService::class.java)

    @MainScope
    @Provides
    fun provideSharedPreferences(context: Context, gson: Gson): WeatherPrefs =
        WeatherPrefsImpl(context, gson)

    @MainScope
    @Provides
    fun provideWeatherRepository(api: WeatherApiService, prefs: WeatherPrefs): WeatherRepository =
        WeatherRepositoryImpl(api, prefs)

    @MainScope
    @Provides
    fun provideWeatherInteractor(weatherRepository: WeatherRepository): WeatherInteractor =
        WeatherInteractorImpl(weatherRepository)

    // добавлено в одном файле, т.к. это сэмпл и особо много здесь провайдить ничего не нужно
    // но в целом, лучше добавить отдельный модуль под VM
    @Module
    interface BindsModule {

        @Binds
        @MainScope
        @IntoMap
        @VmKeyName(MainFragmentVm::class)
        fun bindMainFragmentVm(viewModel: MainFragmentVm): ViewModel

        @Binds
        @MainScope
        fun bindViewModelFactory(factory: VmFactory): ViewModelProvider.Factory

    }
}