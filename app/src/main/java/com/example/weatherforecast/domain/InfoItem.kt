package com.example.weatherforecast.domain

data class InfoItem(
    val id: Long,
    val dateTimeValue: String,
    val temperature: Int
)