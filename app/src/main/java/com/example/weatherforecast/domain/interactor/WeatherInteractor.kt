package com.example.weatherforecast.domain.interactor

import com.example.weatherforecast.domain.InfoItem
import com.example.weatherforecast.domain.repository.WeatherRepository
import com.example.weatherforecast.extensions.formatDateTimeToTime
import com.example.weatherforecast.extensions.formatDateTimeToWeekDay
import com.example.weatherforecast.extensions.nowInSeconds
import io.reactivex.Observable
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

interface WeatherInteractor {
    fun subscribeToChanges(): Observable<Pair<List<InfoItem>, List<InfoItem>>>
}

class WeatherInteractorImpl(private val repository: WeatherRepository) : WeatherInteractor {

    //TODO: добавить экстеншен для форматирования температуры
    override fun subscribeToChanges(): Observable<Pair<List<InfoItem>, List<InfoItem>>> {
        return repository.subscribeToChanges().map { weather ->
            val nowInSeconds = weather.current?.dt ?: nowInSeconds()
            val hourly = weather.hourly
                ?.filter { it -> it.dt?.let { it >= nowInSeconds } ?: false }
                ?.map {
                    formatDateToHour(
                        time = requireNotNull(it.dt), timezone = weather.timezone, temp = it.temp
                    )
                } ?: listOf()
            val daily = weather.daily
                ?.filter { it -> it.dt?.let { it >= nowInSeconds } ?: false }
                ?.map {
                    formatDateToWeek(
                        time = requireNotNull(it.dt), timezone = weather.timezone, temp = it.temp?.day
                    )
                } ?: listOf()
            daily.toMutableList().apply {
                add(
                    0, formatDateToWeek(
                        nowInSeconds, timezone = weather.timezone, temp = weather.current?.temp
                    )
                )
            }
            hourly to daily
        }
    }

    private fun formatDateToHour(time: Long, timezone: String?, temp: Double?): InfoItem {
        val instant = Instant.ofEpochMilli(time * 1000L)
        val ldt = LocalDateTime.ofInstant(instant, ZoneId.of(timezone))
        return InfoItem(
            id = time,
            dateTimeValue = ldt.formatDateTimeToTime() ?: "",
            temperature = (temp?.toInt()?.minus(273)) ?: 0
        )
    }

    private fun formatDateToWeek(time: Long, timezone: String?, temp: Double?): InfoItem {
        val instant = Instant.ofEpochMilli(time * 1000L)
        val ldt = LocalDateTime.ofInstant(instant, ZoneId.of(timezone))
        return InfoItem(
            id = time,
            dateTimeValue = ldt.formatDateTimeToWeekDay() ?: "",
            temperature = (temp?.toInt()?.minus(273)) ?: 0
        )
    }
}