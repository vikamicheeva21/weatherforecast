package com.example.weatherforecast.domain.repository

import android.annotation.SuppressLint
import com.example.weatherforecast.data.api.WeatherApiService
import com.example.weatherforecast.data.models.Weather
import com.example.weatherforecast.data.prefs.weather.WeatherPrefs
import com.example.weatherforecast.extensions.nowInSeconds
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.concurrent.TimeUnit

interface WeatherRepository {
    fun subscribeToChanges(): Observable<Weather>
}

@SuppressLint("CheckResult")
class WeatherRepositoryImpl(private val api: WeatherApiService, private val prefs: WeatherPrefs) :
    WeatherRepository {

    private val subject: BehaviorSubject<Weather> = BehaviorSubject.create()

    //todo добавить AppLifecycle listener, чтобы выключать таймер
    // при уходе приложения с переднего плана
    init {
        val now = nowInSeconds()
        val diff = now - prefs.lastUpdate
        if (diff >= TimeUnit.MINUTES.toSeconds(30))
            requestChanges()
        else {
            prefs.lastWeather?.let { subject.onNext(it) }
        }
        Observable.interval(30, TimeUnit.MINUTES)
            .flatMap { requestChanges() }
            .subscribeOn(Schedulers.io())
            .subscribe({ requestChanges() }, {})
    }


    override fun subscribeToChanges(): Observable<Weather> = subject

    private fun requestChanges(): Observable<Weather> {
        return api.getWeather()
            .doOnSuccess {
                val now = LocalDateTime.now().atZone(ZoneId.of("UTC")).toEpochSecond()
                prefs.lastUpdate = now
                prefs.lastWeather = it
            }
            .toObservable()
    }
}