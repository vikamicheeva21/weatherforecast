package com.example.weatherforecast.extensions

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter


fun LocalDateTime.formatDateTimeToTime(): String? {
    return this.format(DateTimeFormatter.ofPattern("HH:mm"))
}

fun nowInSeconds(): Long {
    return LocalDateTime.now().atZone(ZoneId.of("UTC")).toEpochSecond()
}

fun LocalDateTime.formatDateTimeToWeekDay(): String? {
    return this.format(DateTimeFormatter.ofPattern("EEEE"))
}