package com.example.weatherforecast.presentation.main

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.weatherforecast.R
import com.example.weatherforecast.app.AppComponentsHolder
import com.example.weatherforecast.di.vmBase.VmFactoryWrapper
import com.example.weatherforecast.presentation.main.weaterList.WeatherType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(R.layout.fragment_main) {

    private val vm: MainFragmentVm by lazy {
        ViewModelProvider(this, vmFactoryWrapper.factory)
            .get(MainFragmentVm::class.java)
    }
    private val vmFactoryWrapper = VmFactoryWrapper()
    private val rxBinds: CompositeDisposable = CompositeDisposable()

    private val weekAdapter = InfoItemsAdapter(WeatherType.WEEK)
    private val todayAdapter = InfoItemsAdapter(WeatherType.TODAY)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponentsHolder.getMainComponent().inject(vmFactoryWrapper)
        if (savedInstanceState == null) vm.init()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        todayRv.adapter = todayAdapter
        weekRv.adapter = weekAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        todayRv.adapter = null
        weekRv.adapter = null
    }

    override fun onResume() {
        super.onResume()
        rxBinds.addAll(
            vm.viewState.observeOn(AndroidSchedulers.mainThread())
                .subscribe { render(it) }
        )
    }

    override fun onPause() {
        super.onPause()
        rxBinds.clear()
    }

    private fun render(state: MainState) {
        weekAdapter.list = state.weekList
        todayAdapter.list = state.todayList
        progress.isVisible = state.isLoading
        content.isVisible = !state.isLoading
    }
}