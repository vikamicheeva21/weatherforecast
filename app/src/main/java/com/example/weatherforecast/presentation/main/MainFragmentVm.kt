package com.example.weatherforecast.presentation.main

import androidx.lifecycle.ViewModel
import com.example.weatherforecast.domain.interactor.WeatherInteractor
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainFragmentVm @Inject constructor(
    private val interactor: WeatherInteractor
) : ViewModel() {

    private val rxBinds: CompositeDisposable = CompositeDisposable()
    val viewState = BehaviorRelay.create<MainState>()
    val viewEvents = PublishRelay.create<MainEvent>()

    fun init() {
        rxBinds.addAll(
            viewEvents.subscribe { handleEvents(it) },
            interactor.subscribeToChanges().subscribe {
                viewState.accept(
                    MainState(
                        todayList = it.first,
                        weekList = it.second,
                        isLoading = false
                    )
                )
            }
        )
    }

    private fun handleEvents(it: MainEvent) {
        when (it) {
            is MainEvent.LoadData -> {
                viewState.accept(
                    MainState(
                        todayList = listOf(),
                        weekList = listOf(),
                        isLoading = false
                    )
                )
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        rxBinds.clear()
    }
}