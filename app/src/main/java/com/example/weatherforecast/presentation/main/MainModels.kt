package com.example.weatherforecast.presentation.main

import com.example.weatherforecast.domain.InfoItem

data class MainState(
    val todayList: List<InfoItem> = listOf(),
    val weekList: List<InfoItem> = listOf(),
    val isLoading: Boolean = true
)

sealed class MainEvent {
    object LoadData: MainEvent()
}