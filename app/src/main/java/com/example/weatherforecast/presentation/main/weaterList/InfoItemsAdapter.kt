package com.example.weatherforecast.presentation.main

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherforecast.domain.InfoItem
import com.example.weatherforecast.presentation.main.weaterList.TodayWeatherItemView
import com.example.weatherforecast.presentation.main.weaterList.WeatherItemView
import com.example.weatherforecast.presentation.main.weaterList.WeatherType
import com.example.weatherforecast.presentation.main.weaterList.WeekWeatherItemView


class InfoItemsAdapter(private val type: WeatherType) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var list: List<InfoItem> = listOf()
        set(value) {
            val utilCallback = InfoItemDiffCallback(oldList = field, newList = value)
            val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(utilCallback)
            field = value
            diffResult.dispatchUpdatesTo(this)
        }

    private fun getItem(position: Int) = list[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = when (type) {
            WeatherType.WEEK -> {
                WeekWeatherItemView(parent.context).apply {
                    layoutParams = FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }
            }
            WeatherType.TODAY -> {
                TodayWeatherItemView(parent.context).apply {
                    layoutParams = LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    ).apply { orientation = LinearLayout.VERTICAL }
                }
            }
        }
        return InfoItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val infoItem = getItem(position)
        (holder.itemView as WeatherItemView).bind(infoItem)
    }

    override fun getItemCount(): Int = list.size
}

class InfoItemDiffCallback(
    private val oldList: List<InfoItem>, val newList: List<InfoItem>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}

class InfoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
