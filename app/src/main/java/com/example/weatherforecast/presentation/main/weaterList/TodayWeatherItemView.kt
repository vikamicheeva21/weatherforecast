package com.example.weatherforecast.presentation.main.weaterList

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.weatherforecast.R
import com.example.weatherforecast.domain.InfoItem
import kotlinx.android.synthetic.main.item_weather_today.view.*


class TodayWeatherItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), WeatherItemView {

    init {
        inflate(context, R.layout.item_weather_today, this)
        val outValue = TypedValue()
        context.theme.resolveAttribute(
            android.R.attr.selectableItemBackgroundBorderless, outValue, true
        )
        foreground = ContextCompat.getDrawable(context, outValue.resourceId)
    }

    @SuppressLint("SetTextI18n")
    override fun bind(item: InfoItem) {
        dateTimeTv.text = item.dateTimeValue
        valueTv.text = "${item.temperature}°"
    }
}
