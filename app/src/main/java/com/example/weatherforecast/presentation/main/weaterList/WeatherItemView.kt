package com.example.weatherforecast.presentation.main.weaterList

import com.example.weatherforecast.domain.InfoItem

interface WeatherItemView {
    fun bind(item: InfoItem)
}